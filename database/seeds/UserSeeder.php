<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>"1",
            'name' => "piyawong",
            'lastname' => "mahattanasawat",
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'id'=>"2",
            'name' => "Wachirapat",
            'lastname' => "Mahatubgris",
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'id'=>"3",
            'name' => "kwanchai",
            'lastname' => "limpapanasitti",
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'id'=>"4",
            'name' => "Wakkadet",
            'lastname' => "Panyakamonkit",
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'id'=>"5",
            'name' => "Phakawat",
            'lastname' => "Tapaopong",
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
